FROM maven:3.6-jdk-13 as build
WORKDIR /src
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . .
RUN mvn package
EXPOSE 8181
CMD java -jar target/*.jar
FROM java
COPY . .
